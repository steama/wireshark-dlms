CC=gcc
CFLAGS=-O2 -Wall $(shell pkg-config --cflags-only-I wireshark)

dlms.so: dlms.c
	${CC} ${CFLAGS} -shared -o $@ $^ -s
